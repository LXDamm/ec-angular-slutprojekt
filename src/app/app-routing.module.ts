import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharacterDetailComponent } from './character/character-detail/character-detail.component';
import { GangRosterComponent } from './gang/gang-roster/gang-roster.component';
import { GangsManagerComponent } from './necromunda/gangs-manager/gangs-manager.component';
import { PlayersManagerComponent } from './necromunda/players-manager/players-manager.component';

const routes: Routes = [
  { path: 'player/:playerId/gang/:gangId/character/:characterId', component: CharacterDetailComponent},
  { path: 'player/:playerId/gang/:gangId', component: GangRosterComponent},
  { path: 'player/:playerId', component: GangsManagerComponent},
  { path: '', component: PlayersManagerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
