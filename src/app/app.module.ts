import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GangsManagerComponent } from './necromunda/gangs-manager/gangs-manager.component';
import { GangRosterComponent } from './gang/gang-roster/gang-roster.component';
import { CharacterShortComponent } from './character/character-short/character-short.component';
import { CharacterStatsComponent } from './character/character-stats/character-stats.component';
import { CharacterDetailComponent } from './character/character-detail/character-detail.component';
import { GangDetailComponent } from './gang/gang-detail/gang-detail.component';
import { CharacterNotesComponent } from './character/character-notes/character-notes.component';
import { CharacterStatsEditFormComponent } from './forms/character-stats-edit-form/character-stats-edit-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { GangEditFormComponent } from './forms/gang-edit-form/gang-edit-form.component';
import { CharacterMiscEditFormComponent } from './forms/character-misc-edit-form/character-misc-edit-form.component';
import { CharacterAddFormComponent } from './forms/character-add-form/character-add-form.component';
import { GangAddFormComponent } from './forms/gang-add-form/gang-add-form.component';
import { PlayersManagerComponent } from './necromunda/players-manager/players-manager.component';

@NgModule({
  declarations: [
    AppComponent,
    GangRosterComponent,
    CharacterShortComponent,
    CharacterStatsComponent,
    CharacterDetailComponent,
    GangDetailComponent,
    GangsManagerComponent,
    CharacterNotesComponent,
    CharacterStatsEditFormComponent,
    GangEditFormComponent,
    CharacterMiscEditFormComponent,
    CharacterAddFormComponent,
    GangAddFormComponent,
    PlayersManagerComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
