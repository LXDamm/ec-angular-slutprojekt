import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CharacterMiscEditFormComponent } from 'src/app/forms/character-misc-edit-form/character-misc-edit-form.component';
import { CharacterStatsEditFormComponent } from 'src/app/forms/character-stats-edit-form/character-stats-edit-form.component';
import { ApiService } from 'src/app/services/api.service';
import { Gang } from 'src/app/shared/gang';
import { Character, CharacterShape } from '../../shared/character';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.scss']
})
export class CharacterDetailComponent implements OnInit {
  character?: CharacterShape;
  playerId?: string;
  gangId?: string;
  characterId?: string;

  constructor(public apiService: ApiService, private route: ActivatedRoute, private router: Router, private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.playerId = params.playerId;
      this.gangId = params.gangId;
      this.characterId = params.characterId;
      this.apiService.getCharacter(this.playerId!, this.gangId!, this.characterId!).subscribe((data) => {
        this.character = data;
      },
        (error) => {
          console.log(error);
        });
    });
  }
  editStats(): void {
    const modal = this.modalService.open(CharacterStatsEditFormComponent, { size: 'xl '});
    modal.componentInstance.character = this.character;
    modal.componentInstance.stats = this.character?.stats;
    modal.componentInstance.gangId = this.gangId;
    modal.componentInstance.playerId = this.playerId;
    modal.closed.subscribe((res) => {
      this.apiService.getCharacter(this.playerId!, this.gangId!, this.characterId!).subscribe((character) => {
        this.character = character;
        this.router.navigateByUrl(this.router.url, { skipLocationChange: true});
      },
        (error) => {
          console.log(error);
        });
    });
  }
  editMisc(): void {
    const modal = this.modalService.open(CharacterMiscEditFormComponent, { size: 'xl '});
    modal.componentInstance.character = this.character;
    modal.closed.subscribe((res) => {
      this.apiService.getCharacter(this.playerId!, this.gangId!, this.characterId!).subscribe((character) => {
        this.character = character;
        this.router.navigateByUrl(this.router.url, { skipLocationChange: true});
      },
        (error) => {
          console.log(error);
        });
    });
  }
}
