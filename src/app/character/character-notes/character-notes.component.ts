import { Component, Input, OnInit } from '@angular/core';
import { Character, CharacterShape } from 'src/app/shared/character';
import { Gang } from 'src/app/shared/gang';

@Component({
  selector: 'app-character-notes',
  templateUrl: './character-notes.component.html',
  styleUrls: ['./character-notes.component.scss']
})
export class CharacterNotesComponent implements OnInit {
  @Input() character?: CharacterShape;

  constructor() { }

  ngOnInit(): void {
  }

}
