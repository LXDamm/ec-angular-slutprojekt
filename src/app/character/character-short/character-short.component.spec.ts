import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterShortComponent } from './character-short.component';

describe('CharacterShortComponent', () => {
  let component: CharacterShortComponent;
  let fixture: ComponentFixture<CharacterShortComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharacterShortComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterShortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
