import { Component, Input, OnInit } from '@angular/core';
import { CharacterStatsShape } from '../../shared/character';

@Component({
  selector: 'app-character-stats',
  templateUrl: './character-stats.component.html',
  styleUrls: ['./character-stats.component.scss']
})
export class CharacterStatsComponent implements OnInit {
  @Input() stats?: CharacterStatsShape;

  constructor() { 
    this.stats = {
      m: 0,
      ws: 0,
      bs: 0,
      s: 0,
      t: 0,
      w: 0,
      i: 0,
      a: 0,
      ld: 0,
      cl: 0,
      wil: 0,
      int: 0,
      xp: 0,
    }
  }

  ngOnInit(): void {
  }

}
