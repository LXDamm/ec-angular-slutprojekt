import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/services/api.service';
import { Character, CharacterStatsShape } from 'src/app/shared/character';
import { Gang } from 'src/app/shared/gang';

@Component({
  selector: 'app-character-add-form',
  templateUrl: './character-add-form.component.html',
  styleUrls: ['./character-add-form.component.scss']
})
export class CharacterAddFormComponent implements OnInit {
  @Input() gangId?: string;
  character?: Character;

  playerId?: string;
  characterId?: string;

  form = new FormGroup({
    name: new FormControl(),
    m: new FormControl(),
    ws: new FormControl(),
    bs: new FormControl(),
    s: new FormControl(),
    t: new FormControl(),
    w: new FormControl(),
    i: new FormControl(),
    a: new FormControl(),
    ld: new FormControl(),
    cl: new FormControl(),
    wil: new FormControl(),
    int: new FormControl(),
    xp: new FormControl(),
  });

  constructor(public activeModal: NgbActiveModal, private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.getDefaultCharacter().subscribe((res) => {
      this.character = res;

      (this.form.get('name') as FormControl).setValue(this.character.name);
      (this.form.get('m') as FormControl).setValue(this.character.m);
      (this.form.get('ws') as FormControl).setValue(this.character.ws);
      (this.form.get('bs') as FormControl).setValue(this.character.bs);
      (this.form.get('s') as FormControl).setValue(this.character.s);
      (this.form.get('t') as FormControl).setValue(this.character.t);
      (this.form.get('w') as FormControl).setValue(this.character.w);
      (this.form.get('i') as FormControl).setValue(this.character.i);
      (this.form.get('a') as FormControl).setValue(this.character.a);
      (this.form.get('ld') as FormControl).setValue(this.character.ld);
      (this.form.get('cl') as FormControl).setValue(this.character.cl);
      (this.form.get('wil') as FormControl).setValue(this.character.wil);
      (this.form.get('int') as FormControl).setValue(this.character.int);
      (this.form.get('xp') as FormControl).setValue(this.character.xp);
    });
  }

  createCharacter(): void {
    const createdCharacter = new Character(
      {
        uid: '',
        name: this.form.get('name')?.value,
        imgUrl: '',
        stats: {
          m: this.form.get('m')?.value,
          ws: this.form.get('ws')?.value,
          bs: this.form.get('bs')?.value,
          s: this.form.get('s')?.value,
          t: this.form.get('t')?.value,
          w: this.form.get('w')?.value,
          i: this.form.get('i')?.value,
          a: this.form.get('a')?.value,
          ld: this.form.get('ld')?.value,
          cl: this.form.get('cl')?.value,
          wil: this.form.get('wil')?.value,
          int: this.form.get('int')?.value,
          xp: this.form.get('xp')?.value,
        },
        equipment: '',
        skills: '',
        injuries: '',
        specialRules: '',
        loadout: '',
        notes: ''
      }
    );

    this.apiService.addCharacter(this.playerId!, this.gangId!, this.characterId!, createdCharacter).subscribe((res) => {
      return;
    });
  }
}
