import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterMiscEditFormComponent } from './character-misc-edit-form.component';

describe('CharacterMiscEditFormComponent', () => {
  let component: CharacterMiscEditFormComponent;
  let fixture: ComponentFixture<CharacterMiscEditFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharacterMiscEditFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterMiscEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
