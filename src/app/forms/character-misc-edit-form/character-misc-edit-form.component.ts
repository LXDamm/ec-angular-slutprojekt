import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/services/api.service';
import { Character } from 'src/app/shared/character';
import { Gang } from 'src/app/shared/gang';

@Component({
  selector: 'app-character-misc-edit-form',
  templateUrl: './character-misc-edit-form.component.html',
  styleUrls: ['./character-misc-edit-form.component.scss']
})
export class CharacterMiscEditFormComponent implements OnInit {
  @Input() gang?: Gang;
  @Input() character?: Character;

  form = new FormGroup({
    equipment: new FormControl(),
    skills: new FormControl(),
    injuries: new FormControl(),
    specialRules: new FormControl(),
    loadout: new FormControl(),
    notes: new FormControl(),
  });

  constructor(public activeModal: NgbActiveModal, private apiService: ApiService) { }

  ngOnInit(): void {
    (this.form.get('equipment') as FormControl).setValue(this.character!.equipment);
    (this.form.get('skills') as FormControl).setValue(this.character!.skills);
    (this.form.get('injuries') as FormControl).setValue(this.character!.injuries);
    (this.form.get('specialRules') as FormControl).setValue(this.character!.specialRules);
    (this.form.get('loadout') as FormControl).setValue(this.character!.loadout);
    (this.form.get('notes') as FormControl).setValue(this.character!.notes);
  }

  saveMisc(): void {
    /*this.character!.equipment = this.form.get('equipment')?.value;
    this.character!.skills = this.form.get('skills')?.value;
    this.character!.injuries = this.form.get('injuries')?.value;
    this.character!.specialRules = this.form.get('specialRules')?.value;
    this.character!.loadout = this.form.get('loadout')?.value;
    this.character!.notes = this.form.get('notes')?.value;

    this.apiService.updateCharacterById(this.gang!.uid, this.character!.uid, this.character!);*/
  }
}
