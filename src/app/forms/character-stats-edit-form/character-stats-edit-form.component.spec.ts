import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterStatsEditFormComponent } from './character-stats-edit-form.component';

describe('CharacterStatsEditFormComponent', () => {
  let component: CharacterStatsEditFormComponent;
  let fixture: ComponentFixture<CharacterStatsEditFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharacterStatsEditFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterStatsEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
