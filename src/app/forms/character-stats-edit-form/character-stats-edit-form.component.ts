import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/services/api.service';
import { Character, CharacterShape, CharacterStatsShape } from 'src/app/shared/character';

@Component({
  selector: 'app-character-stats-edit-form',
  templateUrl: './character-stats-edit-form.component.html',
  styleUrls: ['./character-stats-edit-form.component.scss']
})
export class CharacterStatsEditFormComponent implements OnInit {
  @Input() character?: Character;
  @Input() stats?: CharacterStatsShape;

  @Input() playerId?: string;
  @Input() gangId?: string;

  form = new FormGroup({
    name: new FormControl(),
    m: new FormControl(),
    ws: new FormControl(),
    bs: new FormControl(),
    s: new FormControl(),
    t: new FormControl(),
    w: new FormControl(),
    i: new FormControl(),
    a: new FormControl(),
    ld: new FormControl(),
    cl: new FormControl(),
    wil: new FormControl(),
    int: new FormControl(),
    xp: new FormControl(),
  });

  constructor(public activeModal: NgbActiveModal, private apiService: ApiService, public router: Router) { }

  ngOnInit(): void {
    (this.form.get('name') as FormControl).setValue(this.character?.name);
    (this.form.get('m') as FormControl).setValue(this.stats!.m);
    (this.form.get('ws') as FormControl).setValue(this.stats?.ws);
    (this.form.get('bs') as FormControl).setValue(this.stats?.bs);
    (this.form.get('s') as FormControl).setValue(this.stats?.s);
    (this.form.get('t') as FormControl).setValue(this.stats?.t);
    (this.form.get('w') as FormControl).setValue(this.stats?.w);
    (this.form.get('i') as FormControl).setValue(this.stats?.i);
    (this.form.get('a') as FormControl).setValue(this.stats?.a);
    (this.form.get('ld') as FormControl).setValue(this.stats?.ld);
    (this.form.get('cl') as FormControl).setValue(this.stats?.cl);
    (this.form.get('wil') as FormControl).setValue(this.stats?.wil);
    (this.form.get('int') as FormControl).setValue(this.stats?.int);
    (this.form.get('xp') as FormControl).setValue(this.stats?.xp);
  }

  saveStats(): void {
    const updatedCharacter = new Character({
      uid: this.character!.uid,
      name: this.form.get('name')?.value,
      stats: {
        m: this.form.get('m')?.value,
        ws: this.form.get('ws')?.value,
        bs: this.form.get('bs')?.value,
        s: this.form.get('s')?.value,
        t: this.form.get('t')?.value,
        w: this.form.get('w')?.value,
        i: this.form.get('i')?.value,
        a: this.form.get('a')?.value,
        ld: this.form.get('ld')?.value,
        cl: this.form.get('cl')?.value,
        wil: this.form.get('wil')?.value,
        int: this.form.get('int')?.value,
        xp: this.form.get('xp')?.value
      },
      equipment: this.character!.equipment,
      skills: this.character!.skills,
      injuries: this.character!.injuries,
      specialRules: this.character!.specialRules,
      loadout: this.character!.loadout,
      notes: this.character!.notes
    });

    this.apiService.updateCharacter(this.playerId!, this.gangId!, this.character!.uid, updatedCharacter!).subscribe((res) => {
      this.activeModal.close();
    });
  }
}
