import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GangAddFormComponent } from './gang-add-form.component';

describe('GangAddFormComponent', () => {
  let component: GangAddFormComponent;
  let fixture: ComponentFixture<GangAddFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GangAddFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GangAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
