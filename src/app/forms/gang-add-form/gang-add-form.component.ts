import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/services/api.service';
import { Gang } from 'src/app/shared/gang';

@Component({
  selector: 'app-gang-add-form',
  templateUrl: './gang-add-form.component.html',
  styleUrls: ['./gang-add-form.component.scss']
})
export class GangAddFormComponent implements OnInit {
  playerId?: string;
  gang?: Gang;

  form = new FormGroup({
    name: new FormControl(),
    credits: new FormControl(),
    meat: new FormControl(),
    rating: new FormControl(),
    reputation: new FormControl(),
    wealth: new FormControl(),
    notes: new FormControl(),
  });

  constructor(public activeModal: NgbActiveModal, private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.getDefaultGang().subscribe((res) => {
      this.gang = res;

      (this.form.get('name') as FormControl).setValue(this.gang!.name);
      (this.form.get('credits') as FormControl).setValue(this.gang!.credits);
      (this.form.get('meat') as FormControl).setValue(this.gang!.meat);
      (this.form.get('rating') as FormControl).setValue(this.gang!.rating);
      (this.form.get('reputation') as FormControl).setValue(this.gang!.reputation);
      (this.form.get('wealth') as FormControl).setValue(this.gang!.wealth);
      (this.form.get('notes') as FormControl).setValue(this.gang!.notes);
    });
  }

  createGang(): void {
    const tempGang = new Gang(
      {
        uid: '',
        name: this.form.get('name')?.value,
        credits: this.form.get('credits')?.value,
        meat: this.form.get('meat')?.value,
        rating: this.form.get('rating')?.value,
        reputation: this.form.get('reputation')?.value,
        wealth: this.form.get('wealth')?.value
      }
    );

    this.apiService.addGang(this.playerId!, tempGang).subscribe((res) => {
      return;
    });
  }
}
