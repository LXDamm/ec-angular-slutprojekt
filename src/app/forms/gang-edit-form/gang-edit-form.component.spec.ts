import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GangEditFormComponent } from './gang-edit-form.component';

describe('GangEditFormComponent', () => {
  let component: GangEditFormComponent;
  let fixture: ComponentFixture<GangEditFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GangEditFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GangEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
