import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/services/api.service';
import { Gang } from 'src/app/shared/gang';

@Component({
  selector: 'app-gang-edit-form',
  templateUrl: './gang-edit-form.component.html',
  styleUrls: ['./gang-edit-form.component.scss']
})
export class GangEditFormComponent implements OnInit {
  @Input() gang?: Gang;
  playerId?: string;

  form = new FormGroup({
    name: new FormControl(),
    credits: new FormControl(),
    meat: new FormControl(),
    rating: new FormControl(),
    reputation: new FormControl(),
    wealth: new FormControl(),
    notes: new FormControl(),
  });

  constructor(public activeModal: NgbActiveModal, private apiService: ApiService, public router: Router) { }

  ngOnInit(): void {
    (this.form.get('name') as FormControl).setValue(this.gang!.name);
    (this.form.get('credits') as FormControl).setValue(this.gang!.credits);
    (this.form.get('meat') as FormControl).setValue(this.gang!.meat);
    (this.form.get('rating') as FormControl).setValue(this.gang!.rating);
    (this.form.get('reputation') as FormControl).setValue(this.gang!.reputation);
    (this.form.get('wealth') as FormControl).setValue(this.gang!.wealth);
    (this.form.get('notes') as FormControl).setValue(this.gang!.notes);
  }

  saveGang(): void {
    const updatedGang = new Gang(
      {
        uid: this.gang!.uid,
        name: this.form.get('name')?.value,
        credits: this.form.get('credits')?.value,
        meat: this.form.get('meat')?.value,
        rating: this.form.get('rating')?.value,
        reputation: this.form.get('reputation')?.value,
        wealth: this.form.get('wealth')?.value,
        notes: this.form.get('notes')?.value
      }
    );

    this.apiService.updateGang(this.playerId!, this.gang!.uid, updatedGang).subscribe((res) => {
      this.activeModal.close();
    });
  }
}
