import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GangEditFormComponent } from 'src/app/forms/gang-edit-form/gang-edit-form.component';
import { ApiService } from 'src/app/services/api.service';
import { Gang, GangShape } from 'src/app/shared/gang';

@Component({
  selector: 'app-gang-detail',
  templateUrl: './gang-detail.component.html',
  styleUrls: ['./gang-detail.component.scss']
})
export class GangDetailComponent implements OnInit {
  gang?: Gang;
  gangId: any;
  playerId: any;

  constructor(private apiService: ApiService, public modalService: NgbModal, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.playerId = params.playerId;
      this.gangId = params.gangId;
      this.apiService.getGang(this.playerId, this.gangId).subscribe((gang) => {
        this.gang = gang;
      },
        (error) => {
          console.log(error);
        });
    });
  }

  editGang(): void {
    const modal = this.modalService.open(GangEditFormComponent, { size: 'xl '});
    modal.componentInstance.gang = this.gang;
    modal.closed.subscribe((res) => {
      this.apiService.getGang(this.playerId, this.gangId).subscribe((gang) => {
        this.gang = gang;
        this.router.navigateByUrl(this.router.url, { skipLocationChange: true});
      },
        (error) => {
          console.log(error);
        });
    });
  }
}
