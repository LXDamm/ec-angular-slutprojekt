import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GangsManagerComponent } from './gangs-manager.component';

describe('GangsManagerComponent', () => {
  let component: GangsManagerComponent;
  let fixture: ComponentFixture<GangsManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GangsManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GangsManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
