import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GangAddFormComponent } from 'src/app/forms/gang-add-form/gang-add-form.component';
import { ApiService } from 'src/app/services/api.service';
import { Gang, GangShape } from 'src/app/shared/gang';

@Component({
  selector: 'app-gangs-manager',
  templateUrl: './gangs-manager.component.html',
  styleUrls: ['./gangs-manager.component.scss']
})
export class GangsManagerComponent implements OnInit {
  gangs?: Gang[];
  playerId: any;

  constructor(public apiService: ApiService, private route: ActivatedRoute, private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.playerId = params.playerId;
      this.apiService.getGangs(params.playerId).subscribe((data) => {
        this.gangs = data.map((gang: GangShape) => {
          return new Gang(gang);
        });
      },
        (error) => {
          console.log(error);
        });
    });
  }

  addGang(): void {
    const modal = this.modalService.open(GangAddFormComponent, { size: 'xl '});
    modal.componentInstance.playerId = this.playerId;
  }
}
