import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Player, PlayerShape } from 'src/app/shared/player';

@Component({
  selector: 'app-players-manager',
  templateUrl: './players-manager.component.html',
  styleUrls: ['./players-manager.component.scss']
})
export class PlayersManagerComponent implements OnInit {
  players?: Player[];

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.getPlayers().subscribe((data) => {
      this.players = data.map((player: PlayerShape) => {
        return new Player(player);
      });
    },
    (error) => {
      console.log(error);
    });
  }

}
