import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PlayerShape } from 'src/app/shared/player';
import { Character, CharacterShape } from '../shared/character';
import { Gang, GangShape } from '../shared/gang';
import { v4 as uuidv4 } from 'uuid';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private defaultGang = new Gang({ uid: uuidv4(), name: 'Unamed', credits: 1000, meat: 0, rating: 0, reputation: 1, wealth: 1000, characterIds: [] });
  private defaultCharacter = new Character({
    uid: uuidv4(), name: 'Ganger', stats: {
      m: 4,
      ws: 4,
      bs: 3,
      s: 3,
      t: 3,
      w: 1,
      i: 5,
      a: 1,
      ld: 6,
      cl: 7,
      wil: 7,
      int: 6,
      xp: 0
    }, equipment: '', skills: '', injuries: '', specialRules: '', loadout: '', notes: ''
  });

  private apiUrl = 'http://178.73.217.146:7878/api';
  private headers = new Headers();

  constructor(private httpClient: HttpClient) {
    this.headers.append('Content-Type', 'application/json; charset=utf-8');
  }

  protected parsePlayer(data: any): PlayerShape {
    return {
      uid: data.uid,
      name: data.name,
      gangIds: data.gangIds
    };
  }

  protected parseGang(data: any): GangShape {
    return {
      uid: data.uid,
      name: data.name,
      credits: data.credits,
      meat: data.meat,
      rating: data.rating,
      reputation: data.reputation,
      wealth: data.wealth,
      characterIds: data.characterIds,
      notes: data.notes
    };
  }

  protected encodeGang(gang: GangShape) {
    return {
      uid: gang.uid,
      name: gang.name,
      credits: gang.credits,
      meat: gang.meat,
      rating: gang.rating,
      reputation: gang.reputation,
      wealth: gang.wealth,
      characterIds: gang.characterIds,
      notes: gang.notes
    };
  }

  protected parseCharacter(data: any): CharacterShape {
    return {
      uid: data.uid,
      name: data.name,
      imgUrl: data.imgUrl,
      stats: data.stats,
      equipment: data.equipment,
      skills: data.skills,
      injuries: data.injuries,
      specialRules: data.specialRules,
      loadout: data.loadout,
      notes: data.notes
    };
  }

  public getPlayers() {
    const url = `${this.apiUrl}/player`;
    return this.httpClient.get<PlayerShape[]>(url).pipe(map((res) => {
      return res.map((player) => {
        return this.parsePlayer(player);
      });
    }));
  }

  public addGang(playerId: string, createdGang: Gang) {
    const url = `${this.apiUrl}/player/${playerId}/gang`;
    return this.httpClient.post(url, createdGang).pipe((res) => {
      return res;
    });
  }

  public updateGang(playerId: string, gangId: string, updatedGang: Gang) {
    const url = `${this.apiUrl}/player/${playerId}/gang/${gangId}`;
    return this.httpClient.put(url, updatedGang).pipe((res) => {
      return res;
    });    
  }

  public getGangs(playerId: string) {
    const url = `${this.apiUrl}/player/${playerId}/gang`;
    return this.httpClient.get<GangShape[]>(url).pipe(map((res) => {
      return res.map((gang) => {
        return this.parseGang(gang);
      });
    }));
  }

  public getGang(playerId: string, gangId: string) {
    const url = `${this.apiUrl}/player/${playerId}/gang/${gangId}`;
    return this.httpClient.get<GangShape>(url).pipe(map((res) => {
      return this.parseGang(res);
    }));
  }

  public addCharacter(playerId: string, gangId: string, characterId: string, createdCharacter: Character) {
    const url = `${this.apiUrl}/player/${playerId}/gang/${gangId}/character`;
    return this.httpClient.post(url, createdCharacter).pipe((res) => {
      return res;
    });
  }

  public updateCharacter(playerId: string, gangId: string, characterId: string, updatedCharacter: Character) {
    const url = `${this.apiUrl}/player/${playerId}/gang/${gangId}/character/${characterId}`;
    return this.httpClient.put(url, updatedCharacter).pipe((res) => {
      return res;
    });    
  }

  public getCharacters(playerId: string, gangId: string) {
    const url = `${this.apiUrl}/player/${playerId}/gang/${gangId}/character`;
    return this.httpClient.get<CharacterShape[]>(url).pipe(map((res) => {
      return res.map((character) => {
        return this.parseCharacter(character);
      });
    }));
  }

  public getCharacter(playerId: string, gangId: string, characterId: string) {
    const url = `${this.apiUrl}/player/${playerId}/gang/${gangId}/character/${characterId}`;
    return this.httpClient.get<CharacterShape>(url).pipe(map((res) => {
      return this.parseCharacter(res);
    }));
  }

  getDefaultGang(): Observable<Gang> {
    return of(this.defaultGang).pipe();
  }

  getDefaultCharacter(): Observable<Character> {
    return of(this.defaultCharacter).pipe();
  }
}
