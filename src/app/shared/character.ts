import { CharacterStatsComponent } from "../character/character-stats/character-stats.component";

export interface CharacterStatsShape {
    m: number;
    ws: number;
    bs: number;
    s: number;
    t: number;
    w: number;
    i: number;
    a: number;
    ld: number;
    cl: number;
    wil: number;
    int: number;
    xp: number;
}

export interface CharacterShape {
    uid: string;
    name: string;
    imgUrl?: string;
    stats: CharacterStatsShape;
    equipment: string;
    skills: string;
    injuries: string;
    specialRules: string;
    loadout: string;
    notes: string;  
}

export class Character {
    uid: string;
    name: string;
    imgUrl?: string;
    stats: CharacterStatsShape;
    equipment: string;
    skills: string;
    injuries: string;
    specialRules: string;
    loadout: string;
    notes: string; 
    get m() {
        return this.stats.m;
    }
    set m(m: number) {
        this.stats.m = m;
    }
    get ws() {
        return this.stats.ws;
    }
    set ws(ws: number) {
        this.stats.ws = ws;
    }
    get bs() {
        return this.stats.bs;
    }
    set bs(bs: number) {
        this.stats.bs = bs;
    }
    get s() {
        return this.stats.s;
    }
    set s(s: number) {
        this.stats.s = s;
    }
    get t() {
        return this.stats.t;
    }
    set t(t: number) {
        this.stats.t = t;
    }
    get w() {
        return this.stats.w;
    }
    set w(w: number) {
        this.stats.w = w;
    }
    get i() {
        return this.stats.i;
    }
    set i(i: number) {
        this.stats.i = i;
    }
    get a() {
        return this.stats.a;
    }
    set a(a: number) {
        this.stats.a = a;
    }
    get ld() {
        return this.stats.ld;
    }
    set ld(ld: number) {
        this.stats.ld = ld;
    }
    get cl() {
        return this.stats.cl;
    }
    set cl(cl: number) {
        this.stats.cl = cl;
    }
    get wil() {
        return this.stats.wil;
    }
    set wil(wil: number) {
        this.stats.wil = wil;
    }
    get int() {
        return this.stats.int;
    }
    set int(int: number) {
        this.stats.int = int;
    }
    get xp() {
        return this.stats.xp;
    }
    set xp(xp: number) {
        this.stats.xp = xp;
    }
    constructor(character: CharacterShape) {
        this.uid = character.uid;
        this.name = character.name;
        this.stats = character.stats;
        this.equipment = character.equipment;
        this.skills = character.skills;
        this.injuries = character.injuries;
        this.specialRules = character.specialRules;
        this.loadout = character.loadout;
        this.notes = character.notes; 
    }
}
