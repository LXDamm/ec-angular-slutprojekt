import { Character } from "./character";

export interface GangShape {
    uid: string;
    name: string;
    credits: number;
    meat: number;
    rating: number;
    reputation: number;
    wealth: number;
    characterIds?: string[];
    notes?: string;
}

export class Gang {
    uid: string;
    name: string;
    credits: number;
    meat: number;
    rating: number;
    reputation: number;
    wealth: number;
    characterIds?: string[];
    notes?: string;

    constructor(gang: GangShape) {
        this.uid = gang.uid;
        this.name = gang.name;
        this.credits = gang.credits;
        this.meat = gang.meat;
        this.rating = gang.rating;
        this.reputation = gang.reputation;
        this.wealth = gang.wealth;
        this.characterIds = gang.characterIds;
        this.notes = gang.notes;
    }
}
