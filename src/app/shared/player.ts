export interface PlayerShape {
    uid: string;
    name: string;
    gangIds?: string[];
}

export class Player {
    uid: string;
    name: string;
    gangIds?: string[];

    /*constructor(uid: string, name: string, gangIds?: [string]) {
        this.uid = uid;
        this.name = name;
        this.gangIds = gangIds
    }*/

    constructor(player: PlayerShape) {
        this.uid = player.uid;
        this.name = player.name;
        this.gangIds = player.gangIds;
    }
}
